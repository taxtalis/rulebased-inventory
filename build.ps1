$version = Select-String "^#+\s*Version:\s*(.*)$" .\RulebasedInventory.txt | %{ $_.Matches.Groups[1].Value }
$target = "RulebasedInventory_$version.zip"
if (Test-Path $target -PathType Leaf) {
    Write-Host "Target file already exists. Increment the version or delete the target path ($target)."
} else {
    $content = Get-ChildItem -Exclude .git,_classes,*.zip,*.ps1
    $tempPath = Join-Path $Env:Temp $(New-Guid)
    try {
        New-Item -Type Directory -Path $tempPath\RulebasedInventory | Out-Null
        Copy-Item $content -Recurse -Destination $tempPath\RulebasedInventory
        Compress-Archive -Path $tempPath\RulebasedInventory -DestinationPath $target -CompressionLevel Optimal
    }
    finally {
        Remove-Item $tempPath -Recurse
    }
}
