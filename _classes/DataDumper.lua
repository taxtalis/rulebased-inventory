-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: DataDumper.lua
-- File Description: This class is only used to create data exports
-- Load Order Requirements: after RulebasedInventory
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local utils = RbI.class.Utilities

-- GetChampionSkillCurrentBonusText()
-- GetChampionSkillData()
-- GetChampionSkillDescription()
-- GetChampionSkillId()
-- GetChampionSkillJumpPoints()
-- GetChampionSkillLinkIds()
-- GetChampionSkillMaxPoints()
-- GetChampionSkillName()
-- GetChampionSkillPosition()
-- GetChampionSkillType()

local function dumpCSkills()
    local data = {}
    for disciplineIndex = 1, GetNumChampionDisciplines() do
    for skillIndex = 1, GetNumChampionDisciplineSkills(disciplineIndex) do
            local skillId = GetChampionSkillId(disciplineIndex, skillIndex)
            local name = GetChampionSkillName(skillId)
            name = name:gsub('[ \'-]', ''):lower()
            data[#data+1] = name..' = '..skillId..','
        end
    end

    -- TODO: check if we have no double entries
    local dataDump = {}
    dataDump[#dataDump+1] = 'data.cSkill = {'
    for _,value in utils.spairs(data) do
        dataDump[#dataDump+1] = '\t'..value
    end
    dataDump[#dataDump+1] = '}'
    return dataDump
end

local function formatConstant(name, start)
    local result = name:sub(string.len(start)+1):gsub('_', ''):lower()
    if(result == 'invalid') then
        return 'none'
    end
    return result
end

-- @param constant selection via starting string, like 'EQUIP_TYPE_'
local function dumpConstantMappings(...)
    local dataDump = {}
    for _, arg in pairs({...}) do
        local cutStart
        local finder = arg:find("%[")
        if(finder ~= nil) then
            cutStart = arg:sub(1, finder-1)
        else
            cutStart = arg
        end
        d("=> "..arg.."  "..cutStart)
        local constants = {}
        utils.readConstants('^('..arg..'.*)', function(key, value)
            if(not utils.endsWith(key, "_ITERATION_BEGIN") and not utils.endsWith(key, "_MIN_VALUE") and not utils.endsWith(key, "_ITERATION_END") and not utils.endsWith(key, "_MAX_VALUE")) then
                constants[#constants+1] = formatConstant(key, cutStart)..' = '..key..', -- '..tostring(value)
            end
        end)
        cutStart = cutStart:gsub('_', '')
        dataDump[#dataDump+1] = 'data.'..cutStart:lower()..' = {'
        for _,value in utils.spairs(constants) do
            dataDump[#dataDump+1] = '\t'..value
        end
        dataDump[#dataDump+1] = '}'
    end
    return dataDump
end

-- args with regex
local MAX_CONSTANTS = 500
local function dumpConstants(...)
    local constants = {}
    local i = 0
    for _, regex in pairs({...}) do
        utils.readConstants(regex, function(key, value)
            constants[#constants+1] = key..' = '..tostring(value)
            i = i + 1
            if(i >= MAX_CONSTANTS) then
               return false
            end
        end)
    end
    local dataDump = {}
    dataDump[#dataDump+1] = "Entries: "..#constants
    for _,value in utils.spairs(constants) do
        dataDump[#dataDump+1] = '\t'..value
    end
    return dataDump
end

local function dumpConstantSearchForValue(searchValue)
    local constants = {}
    utils.readConstantsAll(function(key, value)
        if(value == searchValue) then
            if(not utils.endsWith(key, "_ITERATION_BEGIN") and not utils.endsWith(key, "_MIN_VALUE") and not utils.endsWith(key, "_ITERATION_END") and not utils.endsWith(key, "_MAX_VALUE")) then
                constants[#constants+1] = key..' = '..tostring(value)
            end
        end
    end)
    local dataDump = {}
    for _,value in utils.spairs(constants) do
        dataDump[#dataDump+1] = '\t'..value
    end
    return dataDump
end

local function printDataType(dataTypeKey)
    local what = RbI.dataType[dataTypeKey]
    local result = {}
    for key, value in pairs(what) do
        result[#result+1] = key..' = '..value
    end
    RbI.PrintToClipBoard(table.concat(result, '\n'))
end

-- statics
local showDataMapping = function(from, to)
    local fromData = RbI.dataTypeStatic[from]
    local toData = RbI.dataTypeStatic[to]
    local error = {}
    if(fromData == nil) then
        error[#error+1] = "FromDataTable can not be found!"
    end
    if(toData == nil) then
        error[#error+1] = "ToDataTable can not be found!"
    end
    if(#error > 0) then
        RbI.PrintToClipBoard(table.concat(error, '\n'))
    else
        RbI.PrintToClipBoard("DataMapping: "..from.." => "..to.."\n"..table.concat(utils.tableMapping(fromData, toData), '\n'))
    end
end
local comparisonTypeNew = 'sType'
local comparisonTypeOld = 'sTypeOld'
RbI.class.DataDumper = {
    {"DataMapping_type", function()
        showDataMapping('typeOld', 'type')
    end},
    {"DataMapping_sType", function()
        showDataMapping('sTypeOld', 'sType')
        --RbI.PrintToClipBoard(table.concat(utils.tableMapping(RbI.dataTypeStatic[comparisonTypeOld], RbI.dataTypeStatic[comparisonTypeNew]), '\n'))
    end},
    {"DataDiff_sType", function()
        d("DataDiff: "..comparisonTypeNew.." <> "..comparisonTypeOld)
        RbI.PrintToClipBoard(table.concat(utils.tableDiff(RbI.dataTypeStatic[comparisonTypeNew], RbI.dataTypeStatic[comparisonTypeOld]), '\n'))
    end},
    {"Dump", function()
        local dynIconTotalCount   = FCOIS.settingsVars.settings.numMaxDynamicIconsUsable
        local dynIconCounter2IconNr = FCOIS.mappingVars.dynamicToIcon
        local maxIconNr = dynIconCounter2IconNr[dynIconTotalCount]
        d(tostring(maxIconNr))
        --RbI.PrintToClipBoard(FCOIS.GetIconText(13))
        --RbI.PrintToClipBoard(table.concat(dumpConstants('^(SI_CHAMPION_.*)$'), '\n'))
        --RbI.PrintToClipBoard(table.concat(dumpConstants('^(SI_CHAMPION_.*)$'), '\n'))
        --RbI.PrintToClipBoard(table.concat(dumpConstants('^(.*CHAMPION.*_ITERATION_BEGIN)$'), '\n'))
        --RbI.PrintToClipBoard(table.concat(dumpConstantSearchForValue(83), '\n'))
    end},
    -- necessary for cSkill-data, because we currently have not CONSTANTS defined for cSkills
    {"CSkills", function()
        RbI.PrintToClipBoard(table.concat(dumpCSkills(), '\n'))
    end},
    {"DataTypes", function()
        RbI.PrintToClipBoard(table.concat(dumpConstantMappings('FCOIS_CON_ICON_', 'INTERACT_TARGET_TYPE_', 'BAG_',
            'CURT_', 'ARMORTYPE_', 'BIND_TYPE_', 'EQUIP_TYPE_', 'ITEM_QUALITY_', 'ITEMSTYLE_', 'ITEM_TRAIT_TYPE_[^(CATEGORY)]',
            'ITEM_TRAIT_TYPE_CATEGORY', 'ITEM_TRAIT_INFORMATION_', 'ITEMTYPE_', 'WEAPONTYPE_', 'SPECIALIZED_ITEMTYPE_'), '\n'))
    end},
    {"ConstantReader", function()
        RbI.constantReader.print()
        RbI.constantReader.read()
    end},
    {"ConsistencyCheck", function()
        RbI.ConsistencyCheck()
    end},
}